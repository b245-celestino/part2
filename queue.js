let collection = [];

// Write the queue functions below.

function print() {
  // It will show the array
  return collection;
}

function enqueue(element) {
  //In this funtion you are going to make an algo that will add an element to the array
  // Mimic the function of push method
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  // In here you are going to remove the last element in the array
  for (let i = 0; i < collection.length; i++) {
    collection[i] = collection[i + 1];
  }
  collection.length = collection.length - 1;
  return collection;
}

function front() {
  // In here, you are going to remove the first element
  return collection[0];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
  // Number of elements

  let element = 0;
  while (collection[element] !== undefined) {
    element++;
  }
  return element;
}

function isEmpty() {
  //it will check whether the function is empty or not
  let emptyQueue = false;
  for (let i = 0; i < size(); i++) {
    if (collection[i] == null) {
      emptyQueue = true;
      return !emptyQueue;
    }
  }
  return emptyQueue;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
